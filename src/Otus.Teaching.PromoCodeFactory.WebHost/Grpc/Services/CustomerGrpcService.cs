﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Grpc.Protos;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Grpc.Services
{
	public class CustomerGrpcService : CustomersGrpcService.CustomersGrpcServiceBase
	{
		private readonly IRepository<Customer> _customerRepository;
		private readonly IRepository<Preference> _preferenceRepository;

		public CustomerGrpcService(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
		{
			_customerRepository = customerRepository;
			_preferenceRepository = preferenceRepository;
		}

		public override async Task<CustomerShortGrpcResponseList> GetCustomersAsync(Empty request, ServerCallContext context)
		{
			var customerShortGrpcResponseList = new CustomerShortGrpcResponseList();

			var customers = await _customerRepository.GetAllAsync();

			foreach (var customer in customers)
			{
				customerShortGrpcResponseList.CustomerShortResponses.Add(new CustomerShortGrpcResponse()
				{
					Id = customer.Id.ToString(),
					Email = customer.Email,
					FirstName = customer.FirstName,
					LastName = customer.LastName,
				});
			}

			return customerShortGrpcResponseList;
		}

		public override async Task<CustomerGrpcResponse> GetCustomerAsync(CustomerId request, ServerCallContext context)
		{
			var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

			if (customer == null)
				return null;

			var customerGrpcResponse = new CustomerGrpcResponse()
			{
					Id = customer.Id.ToString(),
					Email = customer.Email,
					FirstName = customer.FirstName,
					LastName = customer.LastName
			};
			
			if (customer.Preferences.Any())
				customerGrpcResponse.Preferences.AddRange(customer.Preferences.Select(x => new PreferenceGrpcResponse()
					{
						Id = x.PreferenceId.ToString(),
						Name = x.Preference.Name
					}));
			
			return customerGrpcResponse;
		}
		
		public override async Task<Empty> DeleteCustomerAsync(CustomerId request, ServerCallContext context)
		{
			var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

			if (customer == null)
				throw new Exception($"Customer {request.Id} not found");

			await _customerRepository.DeleteAsync(customer);

			return new Empty();
		}

		public override async Task<CustomerGrpcResponse> CreateCustomerAsync(CreateOrEditCustomerGrpcRequest request, ServerCallContext context)
		{
			var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x)).ToList());

			var newCustomerId = Guid.NewGuid();

			var customer = new Customer()
			{
				Id = newCustomerId,
				FirstName = request.FirstName,
				LastName = request.LastName,
				Email = request.Email,
				Preferences = preferences.Select(x => new CustomerPreference()
				{
					CustomerId = newCustomerId,
					Preference = x,
					PreferenceId = x.Id
				}).ToList()
			};

			await _customerRepository.AddAsync(customer);

			return new CustomerGrpcResponse() { Id = customer.Id.ToString() };
		}

		public override async Task<CustomerGrpcResponse> EditCustomerAsync(CreateOrEditCustomerGrpcRequest request, ServerCallContext context)
		{
			var customer = await _customerRepository.GetByIdAsync(System.Guid.Parse(request.Id.ToString()));

			if (customer == null)
				throw new Exception($"Customer {request.Id} not found");

			var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x => System.Guid.Parse(x)).ToList());

			customer.FirstName = request.FirstName;
			customer.LastName = request.LastName;
			customer.Email = request.Email;

			customer.Preferences = preferences.Select(x => new CustomerPreference()
			{
				CustomerId = customer.Id,
				Preference = x,
				PreferenceId = x.Id
			}).ToList();

			await _customerRepository.UpdateAsync(customer);

			var customerGrpc = await GetCustomerAsync(new CustomerId() { Id = request.Id.ToString() }, context);

			return customerGrpc;
		}
	}
}